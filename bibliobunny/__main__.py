#!/usr/bin/python3
from glob import glob
from os.path import basename, splitext, join, isdir, exists, abspath
import json
import click
import pathlib
from .play import parse_google_book_notes_docx
from .kindle import parse_kindle_notes_html
from .post import post_quote
from . import log
from .db import Database
from .quotes import load_quotes_db
import logging
import sys
import random
import time


def __process_notes(files, outdir, overwrite, convert_function):

    log.info(f'Output directory: {outdir}')
    if not isdir(outdir):
        log.info('Output directory does not exist. Creating...')
        pathlib.Path(outdir).mkdir(parents=True)

    for n in files:
        log.info(f'Processing notes {n}')
        basefile = splitext(basename(n))[0]
        outfile = join(outdir, f'{basefile}.json')

        if exists(outfile) and not overwrite:
            log.warning(f'{outfile} exists. Skipping...')
        else:
            book_json = convert_function(n)
            with open(outfile, 'w') as fd:
                fd.write(json.dumps(book_json))


@click.group()
@click.option('--debug', is_flag=True, default=False, help='Enable debug log output')
def main(debug):

    if debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)


@main.command()
@click.option('--outdir', default='output', help='Output directory (will be created if it does not exist)')
@click.option('--overwrite', is_flag=True, default=False, help='Overwrite notes json output')
@click.argument('files', required=True, nargs=-1)
def load_kindle_notes(files, outdir, overwrite):
    __process_notes(files, outdir, overwrite, parse_kindle_notes_html)


@main.command()
@click.option('--outdir', default='output', help='Output directory (will be created if it does not exist)')
@click.option('--overwrite', is_flag=True, default=False, help='Overwrite notes json output')
@click.argument('files', required=True, nargs=-1)
def load_play_notes(files, outdir, overwrite):
    """Process Google Book docx FILE(s)"""
    __process_notes(files, outdir, overwrite, parse_google_book_notes_docx)


@main.command()
@click.option('--sqlitedb', default=join('output', 'bibliobunny-db.sqlite3'), help='SQLite3 file containing bibliobunny db')
@click.option('--instance', envvar='INSTANCE', required=True, help='Instance name (ex: example.social)')
@click.option('--api-token', envvar='API_TOKEN', required=True, help='API authentication token for instance')
@click.option('--interval', envvar='INTERVAL', default=3600, help='Interval between posts in seconds')
@click.pass_context
def postbot_service(ctx, sqlitedb, instance, api_token, interval):
    """Posts random quotes at given interval"""
    while True:
        ctx.invoke(post_random_quote, sqlitedb=sqlitedb, instance=instance, api_token=api_token)
        log.info(f'Waiting {interval} seconds...')
        time.sleep(interval)


@main.command()
@click.option('--sqlitedb', default=join('output', 'bibliobunny-db.sqlite3'), help='SQLite3 file containing bibliobunny db')
@click.option('--instance', envvar='INSTANCE', required=True, help='Instance name (ex: example.social)')
@click.option('--api-token', envvar='API_TOKEN', required=True, help='API authentication token for instance')
def post_random_quote(sqlitedb, instance, api_token):
    """Posts a single random quote, marks the post in the database and exits"""
    db_File = abspath(sqlitedb)
    db = Database(db_File)
    candidates = db.get_quote_candidates()
    if candidates is None:
        log.error('No quote candidates found. Did you run update-quote-db?')
        sys.exit(1)
    else:
        quote = random.choice(candidates)
        url = post_quote(instance, api_token, quote)
        # todo status check
        db.add_post(quote['id'], url)


@main.command()
@click.option('--indir', default='output', help='Input directory containing extracted book highlights with .json extensions')
@click.option('--sqlitedb', default=join('output', 'bibliobunny-db.sqlite3'), help='SQLite3 file containing bibliobunny db')
def update_quote_db(indir, sqlitedb):
    db_File = abspath(sqlitedb)
    db = Database(db_File)
    book_json_files = glob(join(indir, '*.json'))
    load_quotes_db(book_json_files, db)


if __name__ == '__main__':
    main()
