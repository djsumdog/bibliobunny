from yoyo import step

steps = [
    step("""CREATE TABLE posts(
            id INTEGER PRIMARY KEY,
            quote_id INTEGER NOT NULL,
            url TEXT,
            postdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            cycle_num INTEGER,
            FOREIGN KEY (quote_id) REFERENCES quote(id),
            UNIQUE(quote_id, cycle_num)
        );""", "DROP TABLE posts"),
    step("""CREATE TABLE books(
            id INTEGER PRIMARY KEY,
            title TEXT,
            author TEXT,
            UNIQUE(title, author)
        )""", "DROP TABLE book"),
    step("""CREATE TABLE quotes(
            id INTEGER PRIMARY KEY,
            hash TEXT UNIQUE NOT NULL,
            quote TEXT UNIQUE NOT NULL,
            book_id INTEGER,
            location TEXT,
            times_posted INTEGER DEFAULT 0,
            FOREIGN KEY (book_id) REFERENCES book(id)
        )""", "DROP TABLE quotes")
]
