from bs4 import BeautifulSoup
from . import log
import re

page_re = re.compile('(Highlight)(.*)((Page|Location).*)')


def parse_kindle_notes_html(file):

    log.info(f'Processing {file}')
    with open(file, 'r') as fd:
        raw_html = fd.read()

    book_data = {
        'title': None,
        'author': None,
        'publisher': None,
        'notes_string': None,
        'last_synced_string': None,
        'highlights': []
    }

    current_chapter = ''

    soup = BeautifulSoup(raw_html, 'html.parser')
    divs = soup.find_all('div')
    for i, div in enumerate(divs):
        cl = div['class']

        if 'bookTitle' in cl:
            book_data['title'] = div.text.strip()
        elif 'authors' in cl:
            book_data['author'] = div.text.strip()
        elif 'sectionHeading' in cl:
            current_chapter = div.text.strip()
        elif 'noteHeading' in cl:

            if 'Highlight' in div.text:
                location = page_re.match(div.text.strip()).group(3)
                quote = divs[i + 1].text.strip()
                if i + 2 < len(divs) and 'Note' in divs[i + 2].text:
                    note = divs[i + 3].text.strip()
                else:
                    note = None
                book_data['highlights'].append({'quote': quote, 'notes': note,
                                                'page': location, 'chapter': current_chapter})
    return book_data
