import requests
from . import log


def post_quote(instance, key, quote):

    log.info(f"Selected quote {quote['id']} from {quote['title']}")

    if quote['location'].isnumeric():
        cite = f"(p. {quote['location']})"
    else:
        cite = f"({quote['location']})"

    status = f"""> {quote['quote']}\n\n-_{quote['title']}_\n{quote['author']}\n{cite}"""

    url = f'https://{instance}/api/v1/statuses'
    headers = {'Authorization': f'Bearer {key}'}
    payload = {
        'status': status,
        'content_type': 'text/markdown',
        'visibility': 'public'
    }
    res = requests.post(url, headers=headers, data=payload)
    # TODO: Check if valid and report error
    if 'url' not in res.json():
        log.error(f'No URL found in post response: {res.text}')
        post_url = "ERROR"
    else:
        post_url = res.json()["url"]
        log.info(f'Post response code: {res.status_code} URL: {post_url}')
        log.debug(res.text)

    return post_url
