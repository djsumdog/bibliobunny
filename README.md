# BiblioBunny

BiblioBunny is a application for parsing book highlights and notes from various e-readers into a standard format. It currently supports Kindle and Goolge Books. It can also run as a highlight posting bot to a Pleroma server.

* Blog post: [Bibliobunny: A Tool for Extracting eBook Highlights From Kindle and Play Books](https://battlepenguin.com/tech/bibliobunny-a-tool-for-extractin-ebook-highlights-from-kindle-and-play-books/)

### Robot Workflow

If you want to use BiblioBunny as a Book Quote robot, you'll need to go through the following workflow:

* Parse Play/Kindle notes into JSON
* Load JSON into sqlite3 database
* Post from sqlite3 database to Pleroma server

The database keeps track of quotes posted to Pleroma, so quotes aren't repeated.

### Setup

You will need Python and [uv](https://github.com/astral-sh/uv).

```
git clone https://gitlab.com/djsumdog/bibliobunny
cd bibliobunny
uv sync
```

### Parsing Kindle notes

Kindle notes can be exported from their mobile app via the menu.

```
uv run python -m bibliobunny --debug load-kindle-notes kindle-book-note-exports/*html
```

### Parsing Google Play Book notes in docx format

Google Play notes can be found in one's Google Drive and be downloaded in `docx` format.

```
uv run python -m bibliobunny load-play-notes Play\ Books\ Notes/Notes\ from*docx
```

### Other options

The `--overwrite` option can be added to each individual parsing command, otherwise existing json files will be skipped. The `--debug` can be added before the parsing command in order to output verbose logging. Please include the `--debug` option when filing issues

```
uv run python -m bibliobunny --debug load-play-notes Play\ Books\ Notes/Notes\ from*docx --overwrite
```

### Parsing Notes

* Bookmarks are ignored
* Kindle allows for chapter notes without a highlight, which are ignored
* Saving highlight color is not implemented
* Page is stored as an int for Play Books and the string `Location n` for Kindle.


### Loading Note JSON into Sqlite3 Database

```
 uv run python -m bibliobunny update-quote-db
```

### Posting to Pleroma

Posting a single quote

```
env INSTANCE=example.social API_TOKEN=xxxxxxx uv run python -m bibliobunny post-random-quote
```

Starting a service to post quotes at an interval (default: 3600 seconds).

```shell
env INSTANCE=example.social API_TOKEN=xxxxxxx uv run python -m bibliobunny postbot-service
```

### License

GNU AFFERO GENERAL PUBLIC LICENSE v3.0