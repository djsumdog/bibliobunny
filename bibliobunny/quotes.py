import json
from . import log


def load_quotes_db(json_files, db):
    books = []
    for book_file in json_files:
        with open(book_file, 'r') as fd:
            books.append(json.load(fd))
    log.info(f'Loaded {len(books)} books')

    book_rows = db.get_books()
    log.info(book_rows)
    for r in book_rows:
        log.info(r)
    existing_books = {f"{r['title']}-{r['author']}": r for r in book_rows}

    for b in books:
        cur_book_slug = f"{b['title']}-{b['author']}"
        if cur_book_slug not in existing_books:
            book_id = db.add_book(b['title'], b['author'])
            log.info(f'Added Book {cur_book_slug} as {book_id}')
        else:
            book_id = existing_books[cur_book_slug]['id']
            log.info(f'Book {cur_book_slug} exists as {book_id}')

        for h in b['highlights']:
            db.add_quote(book_id, h)
