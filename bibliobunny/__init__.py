import logging
from rich.logging import RichHandler

logging.basicConfig(format='%(message)s', datefmt="[%X]", handlers=[RichHandler()])
log = logging.getLogger("bookquotebot")
