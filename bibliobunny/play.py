import docx
from docx.document import Document
from docx.oxml.table import CT_Tbl
from docx.oxml.text.paragraph import CT_P
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph
from dateutil.parser import parse
import re
from . import log


# Taken from https://github.com/python-openxml/python-docx/issues/85#issuecomment-917134257
def GetParagraphText(paragraph):

    def GetTag(element):
        return "%s:%s" % (element.prefix, re.match("{.*}(.*)", element.tag).group(1))

    text = ''
    runCount = 0
    for child in paragraph._p:
        tag = GetTag(child)
        if tag == "w:r":
            text += paragraph.runs[runCount].text
            runCount += 1
        if tag == "w:hyperlink":
            for subChild in child:
                if GetTag(subChild) == "w:r":
                    text += subChild.text
    return text


Paragraph.text = property(lambda self: GetParagraphText(self))


def is_date(string, fuzzy=False):
    """
    Return whether the string can be interpreted as a date.

    :param string: str, string to check for date
    :param fuzzy: bool, ignore unknown tokens in string if True
    Taken from https://stackoverflow.com/questions/25341945/check-if-string-has-date-any-format
    """
    try:
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False


def parse_google_book_notes_docx(file_name):

    doc = docx.Document(file_name)

    book_data = {
        'title': None,
        'author': None,
        'publisher': None,
        'notes_string': None,
        'last_synced_string': None,
        'highlights': []
    }

    current_chapter = ''

    # source: https://github.com/python-openxml/python-docx/issues/40#issuecomment-440350629
    def iter_block_items(parent):
        if isinstance(parent, Document):
            parent_elm = parent.element.body
        elif isinstance(parent, _Cell):
            parent_elm = parent._tc
        else:
            raise ValueError("Unknown Element in Document Tree")

        for child in parent_elm.iterchildren():
            if isinstance(child, CT_P):
                yield Paragraph(child, parent)
            elif isinstance(child, CT_Tbl):
                table = Table(child, parent)
                for row in table.rows:
                    for cell in row.cells:
                        yield from iter_block_items(cell)

    doc_items = iter_block_items(doc)

    quote = None
    notes = None
    note_date = None
    header = True

    for block in doc_items:
        b = block.text
        style = block.style
        log.debug(f'Element:--{b}--')
        log.debug(style)

        if b is None or b == '':
            pass
        elif 'This document is overwritten when you make' in b:
            pass
        elif 'You should make a copy of this document' in b:
            pass
        elif 'Get it on Google Play' in b:
            pass
        elif 'notes/highlights' in b:
            book_data['notes_string'] = b
        elif 'Last synced' in b:
            last_synced = re.sub(r'^.*?Last synced ', '', b)
            book_data['last_synced_string'] = last_synced
            header = False
        elif header and book_data['title'] is None:
            book_data['title'] = b
        elif header and book_data['author'] is None:
            book_data['author'] = b
        elif header and book_data['publisher'] is None:
            book_data['publisher'] = b
        elif style.name == 'Heading 2':
            current_chapter = b
        elif b.isnumeric():
            if not quote or not note_date:
                log.warning(f"Missing quote or date at page {b} of {book_data['title']}. Is it a bookmark?")
            else:
                book_data['highlights'].append({'quote': quote, 'notes': notes, 'page': b,
                                                'date': note_date, 'chapter': current_chapter})
            quote = None
            notes = None
            note_date = None
        elif is_date(b, fuzzy=False):
            note_date = b
        elif quote is None:
            quote = b
        elif notes is None:
            notes = b
        else:
            log.debug({'quote': quote, 'notes': notes, 'page': b,
                       'date': note_date, 'chapter': current_chapter})
            log.warning(f'Unknown text (style: {style}) {b}')

    return book_data
