from bibliobunny.play import parse_google_book_notes_docx
from bibliobunny.kindle import parse_kindle_notes_html
import unittest
from os.path import join
import json


class PlayBookTests(unittest.TestCase):

    def test_moby_dick(self):
        test_doc = join('tests', 'fixtures', 'play-notes', 'moby_dick.docx')
        result = parse_google_book_notes_docx(test_doc)

        for h in result['highlights']:
            self.assertIsNotNone(h['quote'])

        with open(join('tests', 'fixtures', 'play-notes', 'moby_dick.json'), 'r') as fd:
            json_result = json.load(fd)

        self.assertEqual(result, json_result)


class KindleBookTests(unittest.TestCase):

    def test_locksmith(self):
        test_doc = join('tests', 'fixtures', 'kindle-notes', 'Locksmith (The Lost Demons) - Notebook.html')
        json_file = join('tests', 'fixtures', 'kindle-notes', 'Locksmith (The Lost Demons) - Notebook.json')

        result = parse_kindle_notes_html(test_doc)

        for h in result['highlights']:
            self.assertIsNotNone(h['quote'])

        with open(json_file, 'r') as fd:
            json_result = json.load(fd)

        self.assertEqual(result, json_result)
