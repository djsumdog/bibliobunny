from yoyo import read_migrations
from yoyo import get_backend
import sqlite3
from os import path
from .util import hash_string
from . import log


class Database(object):

    def __init__(self, db_file):
        global log
        self.log = log

        self.log.info(f'Using Database File {db_file}')
        self.db_file = db_file
        self.migrate_schema()
        self.conn = sqlite3.connect(self.db_file)
        self.conn.row_factory = sqlite3.Row

        self.current_cycle = None

        quote_rows = self.run_query('SELECT hash FROM quotes')
        self.quote_hash_cache = [q['hash'] for q in quote_rows]

    def run_query(self, sql, args=[]):
        cursor = self.conn.cursor()
        result = cursor.execute(sql, args)
        self.conn.commit()
        return result.fetchall()

    def run_insert(self, sql, args=[]):
        cursor = self.conn.cursor()
        cursor.execute(sql, args)
        new_id = cursor.lastrowid
        self.conn.commit()
        return new_id

    def migrate_schema(self):
        backend = get_backend('sqlite:///{}'.format(self.db_file))
        ddl_dir = path.join(path.dirname(path.realpath(__file__)), 'ddl')
        migrations = read_migrations(ddl_dir)
        with backend.lock():
            backend.apply_migrations(backend.to_apply(migrations))

    def get_books(self):
        return self.run_query('SELECT id, title, author FROM books')

    def add_book(self, title, author):
        return self.run_insert('INSERT INTO books(title, author) VALUES(?,?)', [title, author])

    def get_current_cycle(self):
        if self.current_cycle is None:
            self.current_cycle = self.run_query('SELECT COALESCE(MAX(cycle_num), 0) AS cycle FROM posts')[0]['cycle']
        return self.current_cycle

    def get_quote_count(self):
        return self.run_query('SELECT COUNT(*) AS count FROM quotes')[0]['count']

    def get_quote_candidates(self):

        quote_count = self.get_quote_count()

        if quote_count == 0:
            return None
        else:
            sql = """SELECT q.id, q.hash, q.quote, q.location, b.title, b.author
                     FROM quotes q LEFT JOIN books b ON q.book_id=b.id
                     WHERE q.id NOT IN (SELECT quote_id FROM posts WHERE cycle_num=?)"""
            pots = self.run_query(sql, [self.current_cycle])
            if len(pots) == 0:
                self.current_cycle += 1
                pots = self.run_query(sql, [self.current_cycle])
                if len(pots) == 0:
                    log.error(f'Database has {quote_count} quotes and increment to cycle {self.current_cycle}, but is returning no quote candidates')
                    raise RuntimeError('Unable to select quote candidates')
            return pots

    def add_quote(self, book_id, quote):
        qhash = hash_string(quote['quote'])
        if qhash in self.quote_hash_cache:
            self.log.info(f"Quote from {quote['page']} exists. Skipping...")
        else:
            self.log.info(f"Adding quote from {quote['page']}")
            self.run_insert('INSERT INTO quotes(hash, quote, book_id, location) VALUES(?, ?, ?, ?)',
                            [qhash, quote['quote'], book_id, quote['page']])
            self.quote_hash_cache.append(qhash)

    def add_post(self, quote_id, url):
        return self.run_insert('INSERT INTO posts(quote_id, url, cycle_num) VALUES(?,?,?)',
                               [quote_id, url, self.current_cycle])
