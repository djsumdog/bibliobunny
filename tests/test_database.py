import tempfile
import unittest
import os

from bibliobunny.db import Database


class DatabaseTest(unittest.TestCase):

    def setUp(self):
        self.sqltemp = tempfile.mkstemp(suffix='.sql')
        self.db = Database(self.sqltemp[1])

    def tearDown(self):
        # print(self.sqltemp[1])
        os.unlink(self.sqltemp[1])

    def __fixtures(self):
        book_a_id = self.db.add_book('The Book of Books', 'Joe Doe')
        book_b_id = self.db.add_book('Not a Book of Books', 'Jamie Doe')

        self.db.add_quote(book_a_id, {'quote': 'The Books of Books has some great quotes',
                                      'page': '10'})
        self.db.add_quote(book_a_id, {'quote': 'But the Book of books only has two quotes',
                                      'page': '25'})

        self.db.add_quote(book_b_id, {'quote': 'Not a Book only has the one not a quote',
                                      'page': 'Location 1000'})

    def test_get_books(self):
        self.__fixtures()
        books = self.db.get_books()
        self.assertEqual(len(books), 2)
        self.assertEqual(books[0]['title'], 'The Book of Books')
        self.assertEqual(books[0]['author'], 'Joe Doe')
        self.assertEqual(books[1]['title'], 'Not a Book of Books')
        self.assertEqual(books[1]['author'], 'Jamie Doe')

    def test_quote_count(self):
        self.assertEqual(self.db.get_quote_count(), 0)
        self.__fixtures()
        self.assertEqual(self.db.get_quote_count(), 3)

    def test_zero_cycle(self):
        self.assertEqual(self.db.get_current_cycle(), 0)

    def test_full_cycle(self):
        self.__fixtures()
        self.assertEqual(self.db.get_current_cycle(), 0)
        self.assertEqual(len(self.db.get_quote_candidates()), 3)

        self.db.add_post(1, 'https://example.com/post/1')
        candidates = self.db.get_quote_candidates()
        self.assertEqual(len(candidates), 2)
        for r in candidates:
            self.assertNotIn(r['id'], [1])

        self.db.add_post(2, 'https://example.com/post/2')
        self.db.add_post(3, 'https://example.com/post/3')
        candidates = self.db.get_quote_candidates()
        self.assertEqual(len(candidates), 3)
        self.assertEqual(self.db.get_current_cycle(), 1)

        self.db.add_post(1, 'https://example.com/post/1')
        self.db.add_post(2, 'https://example.com/post/2')
        self.db.add_post(3, 'https://example.com/post/3')
        candidates = self.db.get_quote_candidates()
        self.assertEqual(len(candidates), 3)
        self.assertEqual(self.db.get_current_cycle(), 2)

    # TODO:
    # def test_add_quote_during_cycle(self):
